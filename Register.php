<?php
	require("password.php");

    $connect = mysqli_connect("mysql.idhostinger.com", "u239175046_odito", "mbahdarmo517", "u239175046_kppl");
    
    $name = $_POST["name"];
    $phonenumber = $_POST["phonenumber"];
    $username = $_POST["username"];
    $password = $_POST["password"];
	
	function registerUser()	{
		global $connect, $name, $phonenumber, $username, $password;
		$passwordHash = password_hash($password, PASSWORD_DEFAULT);
		$statement = mysqli_prepare($connect, "INSERT INTO user (name, username, phonenumber, password) VALUES (?, ?, ?, ?)");
		mysqli_stmt_bind_param($statement, "ssss", $name, $username, $phonenumber, $passwordHash);
		mysqli_stmt_execute($statement);
		mysqli_stmt_close($statement);
    }
	
	function usernameAvailable()	{
		global $connect, $username;
		$statement = mysqli_prepare($connect, "SELECT * FROM user WHERE username = ?");
		mysqli_stmt_bind_param($statement, "s", $username);
		mysqli_stmt_execute($statement);
		mysqli_stmt_store_result($statement);
		$count = mysqli_stmt_num_rows($statement);
		mysqli_stmt_close($statement);
		if($count < 1)	{
			return true;
		}	else	{
			return false;
		}
	}
	
    $response = array();
    $response["success"] = false;  
    
	if(usernameAvailable())	{
		registerUser();
		$response["success"] = true;
	}
	
    echo json_encode($response);
?>
