package com.example.oditouchiha.loginregister;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by oditouchiha on 9/14/2016.
 */
public class RegisterRequest extends StringRequest  {

    private static final String REGISTER_REQUEST_URL = "http://dyaksahanindito.16mb.com/Register.php";
    private Map<String, String> params;

    public RegisterRequest(String name, String username, String phonenumber, String password, Response.Listener<String> listener)  {
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("name", name);
        params.put("username", username);
        params.put("password", password);
        params.put("phonenumber", phonenumber);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
