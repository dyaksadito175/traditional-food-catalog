package com.example.oditouchiha.loginregister;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity /*implements LoginView  */{

    //private EditText etUsername;
    //private EditText etPassword;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        Button bLogin = (Button) findViewById(R.id.bLogin);
        TextView registerLink = (TextView) findViewById(R.id.tvRegisterHere);
        //presenter = new LoginPresenter(this, new LoginService());

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                PopUpDialog popUpDialog = new PopUpDialog();
                popUpDialog.show(manager, "PopUpDialog");
            }
        });

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        //        presenter.onLoginClicked();
                final String username = etUsername.getText().toString();
                final String password = etPassword.getText().toString();
                if (username.isEmpty()) {
                    etUsername.setError(getString(R.string.username_error));
                }
                if (password.isEmpty()) {
                    etPassword.setError(getString(R.string.password_error));
                }

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success) {

                                Intent intent = new Intent(LoginActivity.this, ListResto.class);

                                LoginActivity.this.startActivity(intent);

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setMessage(getString(R.string.login_failed))
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };

                LoginRequest loginRequest = new LoginRequest(username, password, responseListener);
                RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                queue.add(loginRequest);

            }


        });
    }
/*
    @Override
    public String getUsername() {
        return etUsername.getText().toString();
    }

    @Override
    public void showUsernameError(int resId) {

        etUsername.setError(getString(resId));
    }

    @Override
    public String getPassword()
    {
        return etPassword.getText().toString();
    }

    @Override
    public void showPasswordError(int resId)
    {
        etPassword.setError(getString(resId));
    }
    @Override
    public void startMainActivity() {
        Intent intent = new Intent(LoginActivity.this, ListResto.class);
        LoginActivity.this.startActivity(intent);
    }

    @Override
    public void showLoginError(int resId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage(getString(resId))
                .setNegativeButton("Retry", null)
                .create()
                .show();
    }
*/
}
