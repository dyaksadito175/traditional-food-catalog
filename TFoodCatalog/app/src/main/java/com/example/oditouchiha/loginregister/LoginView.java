package com.example.oditouchiha.loginregister;

/**
 * Created by oditouchiha on 11/4/2016.
 */
public interface LoginView {
    String getUsername();

    void showUsernameError(int resId);

    String getPassword();

    void showPasswordError(int resId);

    void startMainActivity();

    void showLoginError(int resId);
}
