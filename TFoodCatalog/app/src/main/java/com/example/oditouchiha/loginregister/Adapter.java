package com.example.oditouchiha.loginregister;

/**
 * Created by oditouchiha on 12/17/2016.
 */

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter extends BaseAdapter implements Filterable{

    Context c;
    ArrayList<Resto> restos;
    CustomFilter filter;
    ArrayList<Resto> filterList;
    public Adapter(Context ctx,ArrayList<Resto> restos) {
        // TODO Auto-generated constructor stub
        this.c=ctx;
        this.restos = restos;
        this.filterList= restos;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return restos.size();
    }
    @Override
    public Object getItem(int pos) {
        // TODO Auto-generated method stub
        return restos.get(pos);
    }
    @Override
    public long getItemId(int pos) {
        // TODO Auto-generated method stub
        return restos.indexOf(getItem(pos));
    }
    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        LayoutInflater inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.item_view, null);
        }
        TextView nameTxt=(TextView) convertView.findViewById(R.id.item_txtName);
        ImageView img=(ImageView) convertView.findViewById(R.id.item_icon);
        TextView ratingTxt =(TextView) convertView.findViewById(R.id.item_txtRating);
        TextView descriptionTxt = (TextView) convertView.findViewById(R.id.item_txtDescription);

        //SET DATA TO THEM
        nameTxt.setText(restos.get(pos).getName());
        img.setImageResource(restos.get(pos).getImg());
        ratingTxt.setText("Rating : " +restos.get(pos).getRating());
        descriptionTxt.setText(restos.get(pos).getDescription());

        return convertView;
    }
    @Override
    public Filter getFilter() {
        // TODO Auto-generated method stub
        if(filter == null)
        {
            filter=new CustomFilter();
        }
        return filter;
    }
    //INNER CLASS
    class CustomFilter extends Filter
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub
            FilterResults results=new FilterResults();
            if(constraint != null && constraint.length()>0)
            {
                //CONSTARINT TO UPPER
                constraint=constraint.toString().toUpperCase();
                ArrayList<Resto> filters=new ArrayList<Resto>();
                //get specific items
                for(int i=0;i<filterList.size();i++)
                {
                    if(filterList.get(i).getName().toUpperCase().contains(constraint))
                    {
                        Resto p=new Resto(filterList.get(i).getName(), filterList.get(i).getImg(), filterList.get(i).getRating(), filterList.get(i).getDescription());
                        filters.add(p);
                    }
                }
                results.count=filters.size();
                results.values=filters;
            }else
            {
                results.count=filterList.size();
                results.values=filterList;
            }
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // TODO Auto-generated method stub
            restos =(ArrayList<Resto>) results.values;
            notifyDataSetChanged();
        }
    }

}
