package com.example.oditouchiha.loginregister;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;

import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;


public class ListResto extends AppCompatActivity {

    ListView lv;
    SearchView sv;
    String[] names={"Depot bu Rudy","Soto cak Har","Sate ayam mas seger"};
    String[] rating = {"5","4.5","4"};
    String[] description = {"Depot atau restoran bu rudy sangat terkenal. Berdiri sejak 14 tahun lalu di kota Surabaya. Pemiliknya adalah ibu Lanny Siswadi atau lebih ...",
            "Bagian pintu masuk utama di Warung Soto Ayam Cak Har. Warung Soto Cak Har berlokasi di daerah Merr, tak jauh dari kampus Institut ...",
            "Depot sate Ponorogo yang berdiri sejak tahun 1976 itu, memiliki sejarah panjang. Awalnya, Pak Seger memulai usahanya sebagai pedagang sate keliling di perkampungan Juwingan, Kertajaya, dan Dharmawangsa."};
    int[] images={R.drawable.depotburudy,R.drawable.sotocakhar,R.drawable.satemasseger};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_resto);
        lv=(ListView) findViewById(R.id.restoListView);
        sv=(SearchView) findViewById(R.id.searchView);
        //ADAPTER
        final Adapter adapter=new Adapter(this, getResto());
        lv.setAdapter(adapter);
        sv.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                // TODO Auto-generated method stub
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }
    private ArrayList<Resto> getResto()
    {
        ArrayList<Resto> resto=new ArrayList<Resto>();
        Resto p;
        for(int i=0;i<names.length;i++)
        {
            p=new Resto(names[i], images[i], rating[i], description[i]);
            resto.add(p);
        }
        return resto;
    }



}
