package com.example.oditouchiha.loginregister;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by oditouchiha on 10/16/2016.
 */
public class Resto /*extends ArrayAdapter<String>*/ {

    private String name;
    private int img;
    private String rating;
    private String description;

    public Resto(String name,int img, String rating, String description) {
        // TODO Auto-generated constructor stub
        this.name=name;
        this.img=img;
        this.rating=rating;
        this.description=description;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getImg() {
        return img;
    }
    public void setImg(int img) {
        this.img = img;
    }
    public String getRating()   {
        return rating;
    }
    public String getDescription()  {
        return description;
    }


}
