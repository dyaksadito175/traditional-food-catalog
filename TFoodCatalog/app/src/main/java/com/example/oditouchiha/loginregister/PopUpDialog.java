package com.example.oditouchiha.loginregister;

import android.app.DialogFragment;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class PopUpDialog extends DialogFragment implements View.OnClickListener {
    Button chooseCustomer, chooseTM;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_pop_up_dialog, null);
        chooseCustomer= (Button) view.findViewById(R.id.buttonChooseCustomer);
        chooseTM= (Button) view.findViewById(R.id.buttonChooseTM);
        chooseCustomer.setOnClickListener(this);
        chooseTM.setOnClickListener(this);
        setCancelable(false);
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.buttonChooseCustomer)    {
            dismiss();
            Intent registerIntent = new Intent(PopUpDialog.this.getActivity(), RegisterActivity.class);
            PopUpDialog.this.startActivity(registerIntent);
        }
        else    {
            dismiss();
            Toast.makeText(getActivity(), "coming soon", Toast.LENGTH_LONG).show();
        }
    }


}
