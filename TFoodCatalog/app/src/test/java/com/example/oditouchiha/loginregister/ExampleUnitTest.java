package com.example.oditouchiha.loginregister;

import android.app.ListActivity;

import com.android.volley.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.doReturn;



/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */

@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    @Mock
    private LoginView view;
    @Mock
    private LoginService service;
    private LoginPresenter presenter;

    private BuildConfig buildConfig;
    private ListResto listResto;
    private LoginActivity loginActivity;
    private LoginRequest loginRequest;
    private PopUpDialog popUpDialog;
    private Resto resto;
    private UserAreaAcivity userAreaAcivity;
    private RegisterActivity registerActivity;
    private SplashScreen splashScreen;

    @Before
    public void setUp() throws Exception {
        presenter = new LoginPresenter(view, service);

    }

    @Test
    public void shouldSplash() throws Exception {
        splashScreen = new SplashScreen();


    }

    @Test
    public void shouldRegisterActivity() throws Exception   {
        registerActivity = new RegisterActivity();
    }

    @Mock
    private EditText mockText;

    @Test
    public void shouldUserActivity() throws Exception   {
        userAreaAcivity = spy(UserAreaAcivity.class);
        doReturn(mockText).when(userAreaAcivity).findViewById(anyInt());
    }

    @Test
    public void shouldResto() throws Exception {
        resto = new Resto("conto",2,"2","true");
        resto.getName();
        resto.getDescription();
        resto.getImg();
        resto.getRating();
    }

    @Test
    public void shouldPopUp() throws Exception {
        popUpDialog = new PopUpDialog();

    }

    @Test
    public void shouldLoginActivity() throws Exception {
        loginActivity = new LoginActivity();
    }

    @Test
    public void shouldlist() throws Exception   {
        listResto = new ListResto();

    }

    @Test
    public void shouldService() throws Exception   {
        service = new LoginService();
        service.login("james", "bond");
    }

    @Test
    public void shouldBuild() throws Exception   {
        buildConfig = new BuildConfig();
    }

    @Test
    public void shouldShowErrorMessageWhenUsernameIsEmpty() throws Exception {
        when(view.getUsername()).thenReturn("");
        presenter.onLoginClicked();

        verify(view).showUsernameError(R.string.username_error);
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordIsEmpty() throws Exception {
        when(view.getUsername()).thenReturn("james");
        when(view.getPassword()).thenReturn("");
        presenter.onLoginClicked();

        verify(view).showPasswordError(R.string.password_error);
    }

    @Test
    public void shouldStartMainActivityWhenUsernameAndPasswordAreCorrect() throws Exception {
        when(view.getUsername()).thenReturn("james");
        when(view.getPassword()).thenReturn("bond");
        when(service.login("james", "bond")).thenReturn(true);
        presenter.onLoginClicked();

        verify(view).startMainActivity();
    }

    @Test
    public void shouldShowLoginErrorWhenUsernameAndPasswordAreInvalid() throws Exception {
        when(view.getUsername()).thenReturn("james");
        when(view.getPassword()).thenReturn("bond");
        when(service.login("james", "bond")).thenReturn(false);
        presenter.onLoginClicked();

        verify(view).showLoginError(R.string.login_failed);
    }

}